import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableDemoComponent } from './table-demo.component';
import { TableComponent } from '../table/table.component';
import { PaginationComponent } from '../table/pagination.component';
import { FormsModule } from '@angular/forms';
import { GenericService } from '../table/generic.service';
import { HttpClientModule } from '@angular/common/http';

describe('TableDemoComponent', () => {
  let component: TableDemoComponent;
  let fixture: ComponentFixture<TableDemoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, FormsModule],
      declarations: [ TableDemoComponent, TableComponent, PaginationComponent ],
      providers: [GenericService],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
