import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-table-demo',
  templateUrl: './table-demo.component.html',
  styleUrls: []
})
export class TableDemoComponent {
  // URL to get the data
  url = './api/data/sample_data.json';
  // Columns to be displayed
  columnDefs = [{
    'name': 'name',
    'width': '200px'
  },
  {
    'name': 'phone',
    'width': '200px'
  },
  {
    'name': 'email',
    'width': '400px'
  },
  {
    'name': 'company',
    'width': '200px'
  },
  {
    'name': 'date_entry',
    'width': '200px'
  },
  {
    'name': 'org_num',
    'width': '200px'
  },
  {
    'name': 'address_1',
    'width': '200px'
  },
  {
    'name': 'city',
    'width': '200px'
  },
  {
    'name': 'zip',
    'width': '200px'
  },
  {
    'name': 'geo',
    'width': '200px'
  },
  {
    'name': 'pan',
    'width': '200px'
  },
  {
    'name': 'pin',
    'width': '200px'
  },
  {
    'name': 'id',
    'width': '50px'
  },
  {
    'name': 'status',
    'width': '100px'
  },
  {
    'name': 'fee',
    'width': '50px'
  },
  {
    'name': 'guid',
    'width': '320px'
  },
  {
    'name': 'date_exit',
    'width': '150px'
  },
  {
    'name': 'date_first',
    'width': '150px'
  },
  {
    'name': 'date_recent',
    'width': '150px'
  },
  {
    'name': 'url',
    'width': '150px'
  }
];
  constructor() { }

}
