import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';


@Injectable()
export class GenericService {

    constructor(private _http: HttpClient) { }

    getData(_Url, page, limit): Observable<any> {
        return this._http.get<any[]>(_Url)
             .map(data => {
                if (limit === undefined) {
                    data = data.slice(0, data.length);
                  } else {
                    data = data.slice((page - 1) * limit, (page - 1) * limit + limit);
                  }
                 return {'data' : data , 'total' : 200};
                 }
                )
            .catch(this.handleError);
    }

    postRowData(rowData): Observable<any[]> {
        return this._http.post('/api/submit', rowData)
            /* .do(data => console.log('All: ' + JSON.stringify(data))) */
            .catch(this.handleError);
    }

    private handleError(err: HttpErrorResponse) {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        let errorMessage = '';
        if (err.error instanceof Error) {
            // A client-side or network error occurred. Handle it accordingly.
            errorMessage = `An error occurred: ${err.error.message}`;
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
        }
        console.error(errorMessage);
        return Observable.throw(errorMessage);
    }
}
