import {   Component,   OnInit,   Input } from '@angular/core';
import {   GenericService } from './generic.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  // The following two inputs are set by the components in which the Table Compoent is used.
  @Input() columnDefs: any[];
  @Input() url: string;

  data: any[];
  errorMessage: string;
  loading = false;
  total: number ;
  page = 1;
  limit = 20; // Default No Of Rows per page

  constructor(private _genericService: GenericService) {}

  ngOnInit() {
    this.getData();
  }

  // This is called when the No of Rows per Page is changed
  limitChanged(n: any): void {
    this.limit = Number(n);
    this.page = 1;
    this.getData();
  }

    // This is called when a specific page no is clicked
  goToPage(n: number): void {
    this.page = n;
    this.getData();
  }

   // This is called when next button is clicked
  onNext(): void {
    this.page++;
    this.getData();
  }

  // This is called when Prevois button is clicked
  onPrev(): void {
    this.page--;
    this.getData();
  }

  // Function to call the service to get data for the table
  getData() {
    this.loading = true;

    this._genericService.getData(this.url, this.page, this.limit)
      .subscribe(results => {
          this.data = results.data;
          this.total = results.total;
          this.loading = false;

        },
        error => {this.errorMessage = < any > error; });
  }

  submitRow(rowData): void {
    this._genericService.postRowData(rowData);
}

}
