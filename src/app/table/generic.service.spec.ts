

import { TestBed, inject } from '@angular/core/testing';

import { GenericService } from './generic.service';
import { HttpClientModule } from '@angular/common/http';

describe('GenericService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GenericService],
      imports : [ HttpClientModule]
    });
  });

  it('should be created', inject([GenericService], (service: GenericService) => {
    expect(service).toBeTruthy();
  }));
});

