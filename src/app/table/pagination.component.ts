import { Component, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent {
  @Input() page: number; // Current Page
  @Input() count: number; // Total No Of Rows
  @Input() _limit: number; // Rows Per Page
  @Input() loading: boolean;
  @Input() pagesToShow: number;

  @Output() goPrev = new EventEmitter<boolean>();
  @Output() goNext = new EventEmitter<boolean>();
  @Output() goPage = new EventEmitter<number>();
  @Output() limitChanged = new EventEmitter<number>();

  constructor() { }

  // Returns the first records index in the page being displayed
  getMin(): number {
    return ((this.limit * this.page) - this.limit) + 1;
  }

  // Returns the last records index in the page being displayed
  getMax(): number {
    let max = this.limit * this.page;
    if (max > this.count) {
      max = this.count;
    }
    return max;
  }

  // Called when a specific page is clicked
  onPage(n: number): void {
    this.goPage.emit(n);
  }

  // Called when previous button is clicked
  onPrev(): void {
    this.goPrev.emit(true);
  }

  // Called when next  button is clicked
  onNext(next: boolean): void {
    this.goNext.emit(next);
  }
  // Called when last  button is clicked
  onLast(): void {
    this.goPage.emit(this.totalPages());
  }
  // Gives the Total No Of pages based on the limit selected
  totalPages(): number {
    return Math.ceil(this.count / this.limit) || 0;
  }

  // Returns is the page displayed is the last page
  lastPage(): boolean {
    return this.limit * this.page >= this.count;
  }

  get limit() {
    return this._limit;
  }

  set limit(value) {
      this._limit = Number(<any>value);
      this.page = 1;
      this.limitChanged.emit(this._limit);
  }

  getPages(): number[] {
    const c = Math.ceil(this.count / this.limit);
    const p = this.page || 1;
    const pagesToShow = this.pagesToShow || 9;
    const pages: number[] = [];
    pages.push(p);
    const times = pagesToShow - 1;
    for (let i = 0; i < times; i++) {
      if (pages.length < pagesToShow) {
        if (Math.min.apply(null, pages) > 1) {
          pages.push(Math.min.apply(null, pages) - 1);
        }
      }
      if (pages.length < pagesToShow) {
        if (Math.max.apply(null, pages) < c) {
          pages.push(Math.max.apply(null, pages) + 1);
        }
      }
    }
    pages.sort((a, b) => a - b);
    return pages;
  }
}
