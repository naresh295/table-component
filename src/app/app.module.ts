import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { TableComponent } from './table/table.component';
import { GenericService } from './table/generic.service';
import { PaginationComponent } from './table/pagination.component';
import { TableDemoComponent } from './table-demo/table-demo.component';

// Define the Routes
const appRoutes: Routes = [
  { path: 'table-demo', component: TableDemoComponent },
  { path: '',  redirectTo: '/table-demo',  pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    TableComponent,
    PaginationComponent,
    TableDemoComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [GenericService],
  bootstrap: [AppComponent]
})
export class AppModule { }
